#+TAGS: export(e) noexport(n)
#+EXCLUDE_TAGS: noexport
#+OPTIONS: toc:t H:2
#+LANGUAGE: en 
#+SELECT_TAGS: export 
#+TITLE: Command Line Tutorial
#+AUTHOR: James Koch
#+EMAIL: jckoch@ualberta.ca
#+DATE: <2017-11-16 Thu>
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [letter,12pt]
#+LATEX_HEADER: \usepackage[margin=0.7in]{geometry} 
#+LATEX_HEADER: \usepackage{float} 
#+LATEX_HEADER: \usepackage{wrapfig} 
#+LATEX_HEADER: \usepackage{amsmath} 
#+LATEX_HEADER: \usepackage{setspace} 
#+LATEX_HEADER: \singlespacing


* Foreword
* Introduction to the Command Line
* TODO Lesson 1: The Basics
* TODO Lesson 2: Scripting
* References						   :ignore_headlines:
bibliographystyle:plain
bibliography:~/Documents/Tutorials/commandlinetutorial/references.bib
